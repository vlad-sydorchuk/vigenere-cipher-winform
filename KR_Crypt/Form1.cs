﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace KR_Crypt
{
    public partial class Form1 : Form
    {
        string alphabet = "0123456789";

        public Form1()
        {
            InitializeComponent();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private string Encrypt(string text, string key)
        {
            string ans = null;
            for (int i = 0; i < text.Length; i++)
                ans += alphabet[(alphabet.IndexOf(text[i]) + alphabet.IndexOf(key[i % key.Length])) % alphabet.Length].ToString();
            return ans;
        }

        private string Decrypt(string text, string key)
        {
            string ans = null;
            for (int i = 0; i < text.Length; i++)
                ans += alphabet[(alphabet.Length+(alphabet.IndexOf(text[i]) - alphabet.IndexOf(key[i % key.Length]))) % alphabet.Length].ToString();
            return ans;
        }

        private bool Num(string text) /* check input isDigit */
        {
            try
            {
                Convert.ToInt64(text);
                return true;
            }
            catch (FormatException) { return false; }
        }

        private void btnRun_Click(object sender, EventArgs e)
        {
            if (txtInput.Text == "")
            {
                MessageBox.Show("You must enter data!");
                return;
            }
            
            txtInput.Text = txtInput.Text.Replace(" ", "");

            if (!Num(txtInput.Text))
            {
                MessageBox.Show("Enter the number, please.");
                return;
            }

            if (txtKey.Text == "")
            {
                MessageBox.Show("Enter the key, please.");
                return;
            }

            if (!Num(txtKey.Text))
            {
                MessageBox.Show("Enter the number for the key, please.");
                return;
            }

            if (radioEncrypt.Checked == true)
                txtOutput.Text = Encrypt(txtInput.Text, txtKey.Text);
            else if (radioDecypt.Checked == true)
                txtOutput.Text = Decrypt(txtInput.Text, txtKey.Text);
            else
                MessageBox.Show("Choose an operation.");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (!Program.flagTb)
            {
                Program.flagTb = true;
                table td = new table();
                td.Show();
            }
            else
                MessageBox.Show("Table is opened.");
        }
    }
}
