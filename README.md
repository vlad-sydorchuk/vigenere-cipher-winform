# Vigenere cipher #

It's a simple realization vigenere cipher.

### How it works ###

1.  Run this project

![Безымянный.png](https://bitbucket.org/repo/9p5AjpL/images/3091100520-%D0%91%D0%B5%D0%B7%D1%8B%D0%BC%D1%8F%D0%BD%D0%BD%D1%8B%D0%B9.png)

2. Input data (only digit, in other case you can see a message) in "input string" and "key".
3. Choose an operation;
4. Press "Go"

![Безымянный.png](https://bitbucket.org/repo/9p5AjpL/images/1585736269-%D0%91%D0%B5%D0%B7%D1%8B%D0%BC%D1%8F%D0%BD%D0%BD%D1%8B%D0%B9.png)


Also, you can see table for vigenere cipher (usual form with image)

![Безымянный.png](https://bitbucket.org/repo/9p5AjpL/images/386198609-%D0%91%D0%B5%D0%B7%D1%8B%D0%BC%D1%8F%D0%BD%D0%BD%D1%8B%D0%B9.png)